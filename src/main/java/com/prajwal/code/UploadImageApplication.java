package com.prajwal.code;

import java.io.File;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.prajwal.code.controller.ImageController;

@SpringBootApplication
public class UploadImageApplication {

	public static void main(String[] args) {
		new File(ImageController.uploadDirectory).mkdir();
		SpringApplication.run(UploadImageApplication.class, args);
	}

}
