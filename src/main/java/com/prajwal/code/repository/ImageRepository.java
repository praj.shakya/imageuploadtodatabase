package com.prajwal.code.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.prajwal.code.entity.Image;

@Repository
public interface ImageRepository extends JpaRepository<Image, Integer> {
	
	@Query(value = "insert into tbl_image_upload(img_name) values(?)", nativeQuery = true)
	@Modifying
	public int insertImageName(String imageName);
	
}
