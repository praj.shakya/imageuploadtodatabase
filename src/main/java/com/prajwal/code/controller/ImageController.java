package com.prajwal.code.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.prajwal.code.repository.ImageRepository;

@Controller
@RequestMapping(value = "/")
public class ImageController {
	@Autowired
	public ImageRepository getImageRepository;
	
	public static String uploadDirectory= System.getProperty("user.dir")+ "/uploads";
	
	@GetMapping
	public String index() {
		
		return "index";
	}
	@PostMapping(value = "/upload")
	@Transactional
	@ResponseBody
	public String uploadImage(Model model, @RequestParam("img_name")MultipartFile file)throws IOException {
		StringBuilder builder=new StringBuilder();
		Path filenameAndPath=Paths.get(uploadDirectory, file.getOriginalFilename());
		builder.append(file.getOriginalFilename());
		Files.write(filenameAndPath,  file.getBytes());
		model.addAttribute("msg", "Image Uploaded Successfully");
		String imageName = file.getOriginalFilename().toString();
		
		getImageRepository.insertImageName(imageName);
		return "ImageUploaded";
		
	}
	
	 
	
//	@GetMapping(value="/image",produces = "image/jpeg")
//	@ResponseBody
//	
//	public byte[] readImage(@RequestParam("id")int id) {
//		return getImageRepository.findById(id).get().getImageName();
//	}
}
